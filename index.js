const express = require("express");
const mongoose = require("mongoose");

const app = express();

const port = 3001;


// [SECTION] MongoDB connection

// Connect to the database by passing in your connection string, remember to replace the password and database names with actual values
mongoose.connect("mongodb+srv://leonor_224:admin123@224-leonor.xr9pdhl.mongodb.net/s35?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}

);

// Connecting to MongoDB locally
let db = mongoose.connection;
// If a connection error occured, output in the console
// "connection error" is the message that will display if an error occured
db.on("error", console.error.bind(console, "connection error"));
// If the connection is successful, output in  the console
db.once("open", () => console.log("Connected to MongoDB"));


// [SECTION] Mongoose Schemas

// Schemas determine the structure of the documents to be written in the database
// "new" creates a new Schema
const taskSchema = new mongoose.Schema({
	// Define ng fields with the corresponding data type
	name: String,
	// There is a field called "status" that is a "String" and the default value is "pending"
	status: {
		type: String,
		default: "pending" 
	}
});


// Models
const Task = mongoose.model("Task", taskSchema);

// [SECTION] Creation of todo list routes
app.use(express.json());
app.use(express.urlencoded({extended: true}));

/*
Business Logic
 Add a functionality to check if there are duplicate tasks
- If the task already exists in the database, we return an error
- If the task doesn't exist in the database, we add it in the database
The task data will be coming from the request's body
Create a new Task object with a "name" field/property
The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/


app.post("/tasks", (req, res) => {
	// "findOne" is a mongoose method that acts similar to "find" of MongoDB
	Task.findOne({name: req.body.name}, (err, result) => {

		// If a document was found and the document's name matches the information sent via the client/Postman
		if(result != null && result.name == req.body.name){
			// Return message to the client/Postman
			return res.send("Duplicate task found!")
		// If no matching document
		}else {
			let newTask = new Task ({
				name: req.body.name
			});
			// "save" method will store the information to the database
			newTask.save((saveErr, savedTask) => {
				// If there are errors in saving
				if(saveErr){
					return console.error(saveErr);
				// No error found while creating the document
				} else {
					// indication that a new task was created
					return res.status(201).send("New task created");
				}
			})
		}
	})
});

// GET all

app.get("/tasks", (req, res) =>{

	// "find" is a mongoose method. With an empty "{}" means it returns all the documents and stires them in the "result" parameter
	Task.find({}, (err, result) => {
		// Will print any errors found in the console
		if(err) {
			return console.log("err");
		} else {
			return res.status(200).json({
			data: result
			})
		}
	})
});

// Activity

const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) =>{
	User.findOne({username: req.body.username}, (err, result) => {
		if(result != null && result.username == req.body.username) {
			return res.send("Username already in use")
		} else {
			let newUser = new User({
				username : req.body.username
			});
			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr)
				} else {
					return res.status(200).send("Congratulations on signing up!")
				}
			})
		}
	})
})


app.listen(port, () => console.log(`Server running at port ${port}`));